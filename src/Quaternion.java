import java.util.*;

/**
 * Quaternions. Basic operations.
 * 
 * @see http://enos.itcollege.ee/~jpoial/algoritmid/quat/
 */
public class Quaternion {

	/** Precision level. */
	public static final double accuracy = 0.000001;

	/** Constructor elements */
	private double real, i, j, k;

	/**
	 * Constructor from four double values.
	 * 
	 * @param a
	 *            real part
	 * @param b
	 *            imaginary part i
	 * @param c
	 *            imaginary part j
	 * @param d
	 *            imaginary part k
	 */
	public Quaternion(double a, double b, double c, double d) {

		this.real = a;
		this.i = b;
		this.j = c;
		this.k = d;

	}

	/**
	 * Real part of the quaternion.
	 * 
	 * @see http://enos.itcollege.ee/~jpoial/algoritmid/quat/
	 * @return real part real
	 */
	public double getRpart() {
		return real;
	}

	/**
	 * Imaginary part i of the quaternion.
	 * 
	 * @see http://enos.itcollege.ee/~jpoial/algoritmid/quat/
	 * @return imaginary part i
	 */
	public double getIpart() {
		return i;
	}

	/**
	 * Imaginary part j of the quaternion.
	 * 
	 * @see http://enos.itcollege.ee/~jpoial/algoritmid/quat/
	 * @return imaginary part j
	 */
	public double getJpart() {
		return j;
	}

	/**
	 * Imaginary part k of the quaternion.
	 * 
	 * @see http://enos.itcollege.ee/~jpoial/algoritmid/quat/
	 * @return imaginary part k
	 */
	public double getKpart() {
		return k;
	}

	/**
	 * Conversion of the quaternion to the string.
	 * 
	 * @return a string form of this quaternion: "a+bi+cj+dk" (without any
	 *         brackets)
	 */
	@Override
	public String toString() {
		return (real + " + " + i + "i + " + j + "j + " + k + "k");
	}

	/**
	 * Conversion from the string to the quaternion. Reverse to
	 * <code>toString</code> method.
	 * 
	 * @throws IllegalArgumentException
	 *             if string s does not represent a quaternion (defined by the
	 *             <code>toString</code> method)
	 * @param s
	 *            string of form produced by the <code>toString</code> method
	 * @return a quaternion represented by string s
	 */
	public static Quaternion valueOf(String s) {
		String[] quat = s.trim().split("\\++");
		
		if (quat.length == 4) {
		double real, i, j, k;
		
			try{
			real = Double.parseDouble(quat[0]);
			i = Double.parseDouble(quat[1].replace("i", " "));
			j = Double.parseDouble(quat[2].replace("j", " "));
			k = Double.parseDouble(quat[3].replace("k", " "));
			
			}
			
			catch (NumberFormatException e){
				
				throw new RuntimeException("Kvaternioon " +s+ " peab sisaldama vaid numbreid!");
			}
			return new Quaternion(real, i, j, k);
		}

		else
			throw new IllegalArgumentException(s + " ei ole kvaterniooni kujul (a+bi+cj+dk).");

	}

	/**
	 * Clone of the quaternion.
	 * 
	 * @see http://enos.itcollege.ee/~jpoial/algoritmid/quat/
	 * @return independent clone of <code>this</code>
	 * @see http://enos.itcollege.ee/~jpoial/algoritmid/quat/
	 */
	@Override
	public Object clone() throws CloneNotSupportedException {
		return new Quaternion(real, i, j, k);
	}

	/**
	 * Test whether the quaternion is zero.
	 * 
	 * @see http://enos.itcollege.ee/~jpoial/algoritmid/quat/
	 * @return true, if the real part and all the imaginary parts are (close to)
	 *         zero
	 */
	public boolean isZero() {
		return equals(new Quaternion(0., 0., 0., 0.));
	}

	/**
	 * Conjugate of the quaternion. Expressed by the formula
	 * conjugate(a+bi+cj+dk) = a-bi-cj-dk
	 * 
	 * @see http://enos.itcollege.ee/~jpoial/algoritmid/quat/
	 * @return conjugate of <code>this</code>
	 */
	public Quaternion conjugate() {
		return new Quaternion(real, -i, -j, -k);
	}

	/**
	 * Opposite of the quaternion. Expressed by the formula opposite(a+bi+cj+dk)
	 * = -a-bi-cj-dk
	 * 
	 * @see http://enos.itcollege.ee/~jpoial/algoritmid/quat/
	 * @return quaternion <code>-this</code>
	 */
	public Quaternion opposite() {
		return new Quaternion(-real, -i, -j, -k);
	}

	/**
	 * Sum of quaternions. Expressed by the formula (a1+b1i+c1j+d1k) +
	 * (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
	 * 
	 * @param q
	 *            addend
	 * @return quaternion <code>this+q</code>
	 * @see http://enos.itcollege.ee/~jpoial/algoritmid/quat/
	 */
	public Quaternion plus(Quaternion q) {

		Quaternion quat = this;
		double a = quat.real + q.real;
		double b = quat.i + q.i;
		double c = quat.j + q.j;
		double d = quat.k + q.k;
		return new Quaternion(a, b, c, d);

	}

	/**
	 * Product of quaternions. Expressed by the formula (a1+b1i+c1j+d1k) *
	 * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
	 * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
	 * 
	 * @param q
	 *            factor
	 * @return quaternion <code>this*q</code>
	 * @see http://introcs.cs.princeton.edu/java/32class/Quaternion.java.html
	 */
	public Quaternion times(Quaternion q) {
		Quaternion quat = this;
		double a = quat.real * q.real - quat.i * q.i - quat.j * q.j - quat.k * q.k;
		double b = quat.real * q.i + quat.i * q.real + quat.j * q.k - quat.k * q.j;
		double c = quat.real * q.j - quat.i * q.k + quat.j * q.real + quat.k * q.i;
		double d = quat.real * q.k + quat.i * q.j - quat.j * q.i + quat.k * q.real;
		return new Quaternion(a, b, c, d);
	}

	/**
	 * Multiplication by a coefficient.
	 * 
	 * @param r
	 *            coefficient
	 * @return quaternion <code>this*r</code>
	 * @see http://enos.itcollege.ee/~jpoial/algoritmid/quat/
	 */
	public Quaternion times(double r) {
		Quaternion quat = this;
		return new Quaternion(quat.real * r, r * i, r * j, r * k);

	}

	/**
	 * Inverse of the quaternion. Expressed by the formula 1/(a+bi+cj+dk) =
	 * a/(a*a+b*b+c*c+d*d) + ((-b)/(a*a+b*b+c*c+d*d))i +
	 * ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
	 * 
	 * @return quaternion <code>1/this</code>
	 */
	public Quaternion inverse() {
		if (isZero()) {
			throw new RuntimeException("Ei saa jagada 0'iga.");
		}

		else {
			return new Quaternion(((real) / (real * real + i * i + j * j + k * k)),
					(-1. * i) / (real * real + i * i + j * j + k * k),
					(-1. * j) / (real * real + i * i + j * j + k * k),
					(-1. * k) / (real * real + i * i + j * j + k * k));

		}
	}

	/**
	 * Difference of quaternions. Expressed as addition to the opposite.
	 * 
	 * @param q
	 *            subtrahend
	 * @return quaternion <code>this-q</code>
	 * @see http://math-it.org/java/docs/org/mathIT/numbers/Quaternion.html
	 */
	public Quaternion minus(Quaternion q) {
		Quaternion quat = this;
		double a = quat.real - q.real;
		double b = quat.i - q.i;
		double c = quat.j - q.j;
		double d = quat.k - q.k;
		return new Quaternion(a, b, c, d);
	}

	/**
	 * Right quotient of quaternions. Expressed as multiplication to the
	 * inverse.
	 * 
	 * @param q
	 *            (right) divisor
	 * @return quaternion <code>this*inverse(q)</code>
	 * @see http://enos.itcollege.ee/~jpoial/algoritmid/quat/
	 */
	public Quaternion divideByRight(Quaternion q) {
		
		if (isZero()) {
			throw new RuntimeException("Ei saa jagada 0'iga.");
		}
		
		else{
		Quaternion quat = this;
		return quat.times(q.inverse());
		}
	}

	/**
	 * Left quotient of quaternions.
	 * 
	 * @param q
	 *            (left) divisor
	 * @return quaternion <code>inverse(q)*this</code>
	 * @see http://enos.itcollege.ee/~jpoial/algoritmid/quat/
	 */
	public Quaternion divideByLeft(Quaternion q) {
		
		if (isZero()) {
			throw new RuntimeException("Ei saa jagada 0'iga.");
		}
		
		else{
		Quaternion quat = this;
		return q.inverse().times(quat);
		}
	}

	/**
	 * Equality test of quaternions. Difference of equal numbers is (close to)
	 * zero.
	 * 
	 * @param real
	 *            real part
	 * @param i
	 *            imaginary part
	 * @param j
	 *            imaginary part
	 * @param k
	 *            imaginary part
	 * @param qo
	 *            second quaternion
	 * @return logical value of the expression <code>this.equals(qo)</code>
	 * @see http://math-it.org/java/docs/org/mathIT/numbers/Quaternion.html
	 */
	@Override
	public boolean equals(Object qo) {

		if (qo instanceof Quaternion) {

			return (Math.abs(real - ((Quaternion) qo).real) < accuracy && Math.abs(i - ((Quaternion) qo).i) < accuracy
					&& Math.abs(j - ((Quaternion) qo).j) < accuracy && Math.abs(k - ((Quaternion) qo).k) < accuracy);
		}

		else {
			return false;
		}

	}

	/**
	 * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
	 * 
	 * @param q
	 *            factor
	 * @return dot product of this and q
	 */
	public Quaternion dotMult(Quaternion q) {
		Quaternion quat = times(q.conjugate()).plus(q.times(conjugate()));

		return new Quaternion(quat.real / 2, quat.i / 2, quat.j / 2, quat.k / 2);
	}

	/**
	 * Integer hashCode has to be the same for equal objects.
	 * 
	 * @return hashcode
	 * @see http://enos.itcollege.ee/~jpoial/algoritmid/quat/
	 */
	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	/**
	 * Norm of the quaternion. Expressed by the formula norm(a+bi+cj+dk) =
	 * Math.sqrt(a*a+b*b+c*c+d*d)
	 * 
	 * @return norm of <code>this</code> (norm is a real number)
	 */
	public double norm() {
		return Math.sqrt(real * real + i * i + j * j + k * k);
	}

	/**
	 * Main method for testing purposes.
	 * 
	 * @param arg
	 *            command line parameters
	 */
	public static void main(String[] arg) {
		Quaternion arv1 = new Quaternion(-1., 1, 2., -2.);
		if (arg.length > 0)
			arv1 = valueOf(arg[0]);
		System.out.println("first: " + arv1.toString());
		System.out.println("real: " + arv1.getRpart());
		System.out.println("imagi: " + arv1.getIpart());
		System.out.println("imagj: " + arv1.getJpart());
		System.out.println("imagk: " + arv1.getKpart());
		System.out.println("isZero: " + arv1.isZero());
		System.out.println("conjugate: " + arv1.conjugate());
		System.out.println("opposite: " + arv1.opposite());
		System.out.println("hashCode: " + arv1.hashCode());
		Quaternion res = null;
		try {
			res = (Quaternion) arv1.clone();
		} catch (CloneNotSupportedException e) {
		}
		;
		System.out.println("clone equals to original: " + res.equals(arv1));
		System.out.println("clone is not the same object: " + (res != arv1));
		System.out.println("hashCode: " + res.hashCode());
		res = valueOf(arv1.toString());
		System.out.println("string conversion equals to original: " + res.equals(arv1));
		Quaternion arv2 = new Quaternion(1., -2., -1., 2.);
		if (arg.length > 1)
			arv2 = valueOf(arg[1]);
		System.out.println("second: " + arv2.toString());
		System.out.println("hashCode: " + arv2.hashCode());
		System.out.println("equals: " + arv1.equals(arv2));
		res = arv1.plus(arv2);
		System.out.println("plus: " + res);
		System.out.println("times: " + arv1.times(arv2));
		System.out.println("minus: " + arv1.minus(arv2));
		double mm = arv1.norm();
		System.out.println("norm: " + mm);
		System.out.println("inverse: " + arv1.inverse());
		System.out.println("divideByRight: " + arv1.divideByRight(arv2));
		System.out.println("divideByLeft: " + arv1.divideByLeft(arv2));
		System.out.println("dotMult: " + arv1.dotMult(arv2));
		Quaternion arv3 = new Quaternion(0., 0., 0., 0.);
		if (arg.length > 1)
			arv2 = valueOf(arg[2]);
		System.out.println("divideByRight: " + arv2.divideByRight(arv3));
		Quaternion arv4 = new Quaternion(3, -2., 4., 2.);
		if (arg.length > 1)
			arv2 = valueOf(arg[2]);
		System.out.println("divideByRight: " + arv2.divideByRight(arv4));
	}
}
// end of file
